﻿mainMenu.comingSoon.title=PRÒXIMAMENT!
mainMenu.comingSoon.moreLevels=Més Nivells
mainMenu.comingSoon.everyWeek=CADA SETMANA!
mainMenu.comingSoon.leaderboards=Classificacions
mainMenu.comingSoon.areYouTheBest=ET CREUS EL MILLOR?
mainMenu.comingSoon.andMore=Y molt més...

mainMenu.loading.throbberHint=Aquesta icona indica que el joc està desant la partida -->
loading.message.saveData=Carregant la partida...
loading.message.social=Connectant a Google Play Game Services...
loading.message.facebook=Connectant a Facebook...

mainMenu.touchTo=Toca per a
mainMenu.start=Començar
mainMenu.exit=Sortir

error.socialAuto.title=La connexió automàtica a {0} ha fallat
error.social.title=Error connectant a {0}
error.social.retry=Tornar a intentar
error.social.cancel=Cancelar

social.logout.title=Ja estás connectat a {0}.
social.logout.title=Desconnectar?
social.logout.yes=Desconnectar
social.logout.cancel=Cancelar

social.single.title=This game can't use the two Social networks at the same time.
social.single.title=What do you want to do?
social.single.yes=Switch to {0}
social.single.cancel=Continue with {0}

social.choice.title=Cloud backup requires a Social connection to identify you in the cloud.
social.choice.title=What do you want to do?
social.choice.gp=Connect with Google
social.choice.fb=Connect with Facebook
social.choice.cancel=Play Offline

save.overwrite.title=Local data is for a different {0} account. Overwrite?
save.overwrite.overwrite=Overwrite Local Data
save.overwrite.cancel=Cancel Login

error.savedata.title=Error carregant dades de partida
error.savedata.retry=Tornar a Intentar
error.savedata.cancel=Crear Partida Nova

error.adsError.title=No s'ha pogut mostrar l'anunci: {0}
error.adsError.notInit=El sistema UnityAds no s'ha inicialitzat.
error.adsError.failed=La operació ha fallat.
error.adsError.ok=Acceptar

error.undoLimitAd.title=Has arribat al límit de desfer per aquesta partida, però pots ampliar aquest límit durant una estona si mires un anunci.
error.undoLimitAd.ok=Veure Anunci
error.undoLimitAd.cancel=Cancelar

error.tooManyAds.title=Has vist molts anuncis en les ultimes 24h, mentres segueixi aquesta sicuació, el límit de desfer romandrà desbloquejat.
error.tooManyAds.ok=Acceptar

prompt.notUnlocked.title=Aquest nivell no està desbloquejat.
prompt.notUnlocked.title=Consegueix {0} peces de llum més per tal de desbloquejar-lo.
prompt.notUnlocked.ok=Acceptar

save.merge.title=Hi ha dades descarregades que no coincideixen amb la informació local.
save.merge.title=Que desitja fer?
save.merge.merge=Unificar les partides
save.merge.overwrite=Sobreescriure
save.merge.cancel=Cancelar Descàrrega

support.countdown.timer=Continuar en {0}
support.countdown.after=Continuar
support.watchAd=Veure Anunci
support.message.top=Aquest joc gratuït ha estat creat per donar suport al meu joc principal: Rolling Light.
support.message.bottom=Si vols ajudar al desenvolupament, pots veure un anunci ara, o visitar la pàgina per a més informació.
support.visitSite=Visitar Web

mainMenu.button.rate=Valorar
mainMenu.button.achievements=Assoliments
mainMenu.button.facebook=Connectar
mainMenu.button.googlePlay=Connectar
mainMenu.button.facebook2=Desconnectar
mainMenu.button.googlePlay2=Desconnectar

levelMenu.bestScore=Millor Puntuació: {0}
levelMenu.play=Jugar!
levelMenu.comingSoon=MÉS NIVELLS PRÒXIMAMENT!
levelMenu.back=Back

overlay.music.track=Cançó: {0}

menu.pause.title=Joc en Pausa
menu.pause.restart=Tornar a començar
menu.pause.undo=Desfer
menu.pause.resume=Continuar
menu.pause.exit=Sortir al Menú
menu.pause.chargesAvilable={0} càrregues disponibles
menu.pause.chargesUsed={0}/{1} utilitzades en aquest nivell
menu.pause.tooManyAds=Massa anuncis! Desfer bloquejat fins nou avís.
menu.pause.chargeTime={0:00}:{1:00} restant amb càrregues extra
menu.pause.chargeExtraAvailable=Càrregues extra disponibles!

menu.win.title=¡Nivell Completat!
menu.win.tryAgain=Tornar a Intentar
menu.win.next=Següent Nivell
menu.win.exit=Sortir al Menú
menu.win.score=Puntuació: {0}

menu.lose.title=Fi de la Partida
menu.lose.tryAgain=Tornar a Intentar
menu.lose.exit=Sortir al Menú

tutorial.ball=Aquesta és la bola.
tutorial.ball=Arrossega-la enrere
tutorial.ball=per a llançar-la.
tutorial.light=Aquesta és la teva llum,
tutorial.light=es consumeix al llançar.
tutorial.light=¿Veus com s'esvaeix quan arrosegues?
tutorial.end=Aquest és el final del nivell.
tutorial.end=Apunta per a acabar.
tutorial.pieces=Aquestes són les peces de llum.
tutorial.pieces=Recarreguen la bola.
tutorial.pieces=Intenta aconseguir-les totes.
tutorial.choices=A vegades hi ha més
tutorial.choices=d'un sol camí.
tutorial.choices=Tria amb seny.
tutorial.angle=El final està molt lluny en aquest nivell.
tutorial.angle=Llança amb un angle al voltant de 45°
tutorial.angle=per a maximitzar la distància.
tutorial.throws=Encara que utilitzis la força màxima,
tutorial.throws=no sempre és possible arribar prou lluny.
tutorial.throws=Llança un altre cop per seguir endavant.
tutorial.water=Això és Aigua. Ves amb compte,
tutorial.water=no podràs sortir un cop caus a dins.

level.W0L1=Primers passos
level.W0L2=Creix
level.W0L3=Opcions
level.W0L4=Torna a tirar
level.W0L5=Vida
level.W1L1=Arrossega y deixa anar
level.W1L2=Controla't
level.W1L3=No passa res si falles
level.W1L4=Prova de nou
level.W1L5=Mira atentament
level.W2L1=Noves eines
level.W2L2=Tria amb seny
level.W2L3=Pensa endavant
level.W2L4=Continuar
level.W2L5=Camins invisibles
level.W3L1=Corrents
level.W3L2=Final ventós
level.W3L3=Salt de longitud
level.W3L4=A l'altra banda
level.W3L5=Tria el teu repte
level.W4L1=No caiguis
level.W4L2=De debò, amb compte
level.W4L3=No miris a baix
level.W4L4=Amunt
level.W4L5=Vertigen

# Not yet seen in the game
endOfGame.watchAd=Veure Anunci
endOfGame.message=Felicitats!
endOfGame.message=Has acabat tots els nivells disponibles actualment.
endOfGame.message=Sisplau, aprofita aquest moment per ajudar al desenvolupament del meu joc principal: Rolling Light.
endOfGame.visitSite=Visitar Web
endOfGame.levelMenu=Menú de Nivells
