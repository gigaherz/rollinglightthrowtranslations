mainMenu.comingSoon.title=COMING SOON!
mainMenu.comingSoon.moreLevels=More Levels
mainMenu.comingSoon.everyWeek=EVERY WEEK!
mainMenu.comingSoon.leaderboards=Leaderboards
mainMenu.comingSoon.areYouTheBest=ARE YOU THE BEST?
mainMenu.comingSoon.andMore=And more...

mainMenu.loading.throbberHint=This indicator means the game is saving -->
loading.message.saveData=Loading save data...
loading.message.social=Connecting to Google Play Game Services...
loading.message.facebook=Connecting to Facebook...

mainMenu.touchTo=Touch to
mainMenu.start=Start
mainMenu.exit=Exit

error.socialAuto.title=Automatic connection to {0} failed
error.social.title=Error conecting to {0}
error.social.retry=Try Again
error.social.cancel=Cancel

social.logout.title=You are already logged into {0}.
social.logout.title=Log out?
social.logout.yes=Log Out
social.logout.cancel=Cancel

social.single.title=This game can't use the two Social networks at the same time.
social.single.title=What do you want to do?
social.single.yes=Switch to {0}
social.single.cancel=Continue with {0}

social.choice.title=Cloud backup requires a Social connection to identify you in the cloud.
social.choice.title=What do you want to do?
social.choice.gp=Connect with Google
social.choice.fb=Connect with Facebook
social.choice.cancel=Play Offline

save.overwrite.title=Local data is for a different {0} account. Overwrite?
save.overwrite.overwrite=Overwrite Local Data
save.overwrite.cancel=Cancel Login

error.savedata.title=Error loading Save Game data
error.savedata.retry=Try Again
error.savedata.cancel=Create New Save

error.adsError.title=Could not show an ad: {0}
error.adsError.notInit=The UnityAds system was not initialized.
error.adsError.failed=The operation failed.
error.adsError.ok=Ok

error.undoLimitAd.title=You have reached the maximum undos for this level, but you can extend the limit for a while by watching a Video Ad.
error.undoLimitAd.ok=Play Ad
error.undoLimitAd.cancel=Cancel

error.tooManyAds.title=You have watched a lot of Ads in the last 24h, during this situation, your undos will remain unlocked.
error.tooManyAds.ok=Ok

prompt.notUnlocked.title=This level has not been unlocked yet.
prompt.notUnlocked.title=Gather {0} more pieces of light to unlock.
prompt.notUnlocked.ok=Ok

save.merge.title=There's downloaded save data that doesn't match the local save.
save.merge.title=What do you want to do?
save.merge.merge=Merge Save Data
save.merge.overwrite=Overwrite Local Data
save.merge.cancel=Cancel Download

support.countdown.timer=Continue in {0}
support.countdown.after=Continue
support.watchAd=Watch a Video Ad
support.message.top=This free game was created to support development of my bigger game: Rolling Light.
support.message.bottom=If you want to help support the game, watch video advertisement, or visit the game's website for more information.
support.visitSite=Visit Website

mainMenu.button.rate=Rate & Review
mainMenu.button.achievements=Achievements
mainMenu.button.facebook=Connect
mainMenu.button.googlePlay=Connect
mainMenu.button.facebook2=Disconnect
mainMenu.button.googlePlay2=Disconnect

levelMenu.bestScore=Best Score: {0}
levelMenu.play=Play!
levelMenu.comingSoon=MORE LEVELS COMING SOON!
levelMenu.back=Back

overlay.music.track=Track: {0}

menu.pause.title=Game Paused
menu.pause.resume=Resume
menu.pause.undo=Undo
menu.pause.restart=Restart
menu.pause.exit=Exit to Menu
menu.pause.chargesAvilable={0} charges available now
menu.pause.chargesUsed={0}/{1} charges used this level
menu.pause.tooManyAds=Too Many Ads! Undos unlocked until further notice.
menu.pause.chargeTime={0:00}:{1:00} remaining with extra charges
menu.pause.chargeExtraAvailable=Extra charges available!

menu.win.title=Level finished!
menu.win.score=Score: {0}
menu.win.next=Next Level
menu.win.tryAgain=Try Again
menu.win.exit=Exit to Menu

menu.lose.title=Game Over
menu.lose.tryAgain=Try Again
menu.lose.exit=Exit to Menu

tutorial.ball=This is the ball.
tutorial.ball=Drag it back to throw.
tutorial.light=This is your light,
tutorial.light=you spend it to throw.
tutorial.light=See how it fades when you drag?
tutorial.end=This is the end of the level.
tutorial.end=Aim for it to finish.
tutorial.pieces=These are pieces of light.
tutorial.pieces=They recharge the ball.
tutorial.pieces=Try to get them all.
tutorial.choices=Sometimes there's more
tutorial.choices=than one path.
tutorial.choices=Choose wisely.
tutorial.angle=The end is far away in this level.
tutorial.angle=Throw at approximately 45° angle
tutorial.angle=to maximize the distance.
tutorial.throws=Even with maximum force,
tutorial.throws=it's always not far enough.
tutorial.throws=Throw again to reach further away.
tutorial.water=This is Water. Be careful,
tutorial.water=you can't get out once you fall in.

level.W0L1=First Steps
level.W0L2=Grow
level.W0L3=Choices
level.W0L4=Throw Again
level.W0L5=Life
level.W1L1=Drag and release
level.W1L2=Control yourself
level.W1L3=it's ok to fail
level.W1L4=Just throw again
level.W1L5=Look carefully
level.W2L1=New tools
level.W2L2=Choose wisely
level.W2L3=Think forward
level.W2L4=Continue
level.W2L5=Invisible Paths
level.W3L1=Currents
level.W3L2=Windy ending
level.W3L3=Long jump
level.W3L4=On the other side
level.W3L5=Choose your challenge
level.W4L1=Don't fall
level.W4L2=Really, don't
level.W4L3=Don't look down
level.W4L4=Up
level.W4L5=Vertigo

# Not yet seen in the game
endOfGame.watchAd=Watch a Video Ad
endOfGame.message=Congratulations!
endOfGame.message=You finished all the available levels.
endOfGame.message=Please take a moment to support the development of my bigger game: Rolling Light.
endOfGame.visitSite=Visit Website
endOfGame.levelMenu=Level Menu
