﻿mainMenu.comingSoon.title=PROCHAINEMENT!
mainMenu.comingSoon.moreLevels=Plus de niveaux
mainMenu.comingSoon.everyWeek=CHAQUE SEMAINE!
mainMenu.comingSoon.leaderboards=Scores
mainMenu.comingSoon.areYouTheBest=VOUS PENSEZ QUE VOUS ÊTES LE MEILLEUR??
mainMenu.comingSoon.andMore=Et bien plus encore...

mainMenu.loading.throbberHint=Cette icône indique que le jeu est sauvé -->
loading.message.saveData=Chargement du jeu...
loading.message.social=Connectant à Google Play Game Services...
loading.message.facebook=Connecting to Facebook...

mainMenu.touchTo=Touchez pour
mainMenu.start=Commencer
mainMenu.exit=Exit

error.socialAuto.title=Automatic connection to {0} failed
error.social.title=Error conecting to {0}
error.social.retry=Try Again
error.social.cancel=Cancel

social.logout.title=You are already logged into {0}.
social.logout.title=Log out?
social.logout.yes=Log Out
social.logout.cancel=Cancel

social.single.title=This game can't use the two Social networks at the same time.
social.single.title=What do you want to do?
social.single.yes=Switch to {0}
social.single.cancel=Continue with {0}

social.choice.title=Cloud backup requires a Social connection to identify you in the cloud.
social.choice.title=What do you want to do?
social.choice.gp=Connect with Google
social.choice.fb=Connect with Facebook
social.choice.cancel=Play Offline

save.overwrite.title=Local data is for a different {0} account. Overwrite?
save.overwrite.overwrite=Overwrite Local Data
save.overwrite.cancel=Cancel Login

error.savedata.title=Erreur de chargement des données du jeu
error.savedata.retry=Réessayez
error.savedata.cancel=Créer un nouveau jeu

error.adsError.title=Impossible d'afficher l'annonce: {0}
error.adsError.notInit=Le système UnityAds n'a pas été initialisé.
error.adsError.failed=L'opération a échoué.
error.adsError.ok=Accepter

error.undoLimitAd.title=Vous avez atteint la limite de défaires pour ce jeu, mais vous pouvez étendre la limite pendant un certain temps si vous regardez une annonce.
error.undoLimitAd.ok=Ver Annonce
error.undoLimitAd.cancel=Annuler

error.tooManyAds.title=Vous avez vu beaucoup de publicités dans les dernières 24h, dans cette situation, l'option Annuler restera débloqué.
error.tooManyAds.ok=Ok

prompt.notUnlocked.title=Ce niveau n'est pas débloqué.
prompt.notUnlocked.title=Obtenez {0} morceaux de lumière plus pour débloquer.
prompt.notUnlocked.ok=Ok

save.merge.title=There's downloaded save data that doesn't match the local save.
save.merge.title=What do you want to do?
save.merge.merge=Merge Save Data
save.merge.overwrite=Overwrite Local Data
save.merge.cancel=Cancel Download

support.countdown.timer=Continuer en {0}
support.countdown.after=Continuer
support.watchAd=Ver Annonce
support.message.top=Ce jeu gratuit a été créé pour soutenir le développement de mon jeu principal: Rolling Light.
support.visitSite=Visiter Web
support.message.bottom=Si vous voulez aider, regardez l'annonce aujourd'hui, ou visitez la page du jeu pour plus d'informations.

mainMenu.button.rate=Rate & Review
mainMenu.button.achievements=Achievements
mainMenu.button.facebook=Connect
mainMenu.button.googlePlay=Connect
mainMenu.button.facebook2=Disconnect
mainMenu.button.googlePlay2=Disconnect

levelMenu.bestScore=Meilleur score: {0}
levelMenu.play=Jouer!
levelMenu.comingSoon=PLUS DE NIVEAUX PROCHAINEMENT!
levelMenu.back=Back

overlay.music.track=Chanson: {0}

menu.pause.title=Jeu en pause
menu.pause.restart=Recommencer
menu.pause.undo=Défaire
menu.pause.resume=Continuer
menu.pause.exit=Retour au menu
menu.pause.chargesAvilable={0} charges disponibles
menu.pause.chargesUsed={0}/{1} utilisées à ce niveau
menu.pause.tooManyAds=Trop d'annonces! Annuler défaire jusqu'à nouvel ordre.
menu.pause.chargeTime={0:00}:{1:00} restant avec des charges extra
menu.pause.chargeExtraAvailable=Charges extra disponibles!

menu.win.title=Niveau complété!
menu.win.tryAgain=Réessayez
menu.win.next=Prochain niveau
menu.win.exit=Retour au menu
menu.win.score=Score: {0}

menu.lose.title=Fin du jeu
menu.lose.tryAgain=Réessayez
menu.lose.exit=Retour au menu

tutorial.ball=Cela est la balle.
tutorial.ball=Faites glisser vers l'arrière
tutorial.ball=pour lancer.
tutorial.light=Cela est votre lumière,
tutorial.light=les dépenses pour le lancement.
tutorial.light=Voyez comment se fane à faire glisser?
tutorial.end=Ce est la fin du niveau.
tutorial.end=Apunta para terminar.
tutorial.pieces=Ce sont des morceaux de lumière.
tutorial.pieces=Rechargent la balle.
tutorial.pieces=Essayez de les obtenir tous.
tutorial.choices=Parfois, il ya plus
tutorial.choices=d'un chemin.
tutorial.choices=Choisissez judicieusement.
tutorial.angle=La fin est très loin à ce niveau.
tutorial.angle=Lance avec un angle d'environ 45°
tutorial.angle=pour maximiser la distance.
tutorial.throws=Même si vous utilisez la force maximale,
tutorial.throws=est pas toujours possible d'aller aussi loin.
tutorial.throws=Tira otra vez para seguir adelante.
tutorial.water=Ceci est l'eau. Avancer prudemment,
tutorial.water=vous ne pouvez pas sortir une fois que vous tombez dans.

level.W0L1=Premiers pas
level.W0L2=Grandit
level.W0L3=Options
level.W0L4=Vuelve a tirar
level.W0L5=Vie
level.W1L1=Glisser et déposer
level.W1L2=Contrôlez-vous
level.W1L3=Rien ne se passe si vous échouez
level.W1L4=Essayez à nouveau
level.W1L5=Regardez attentivement
level.W2L1=Nouveaux outils
level.W2L2=Choisissez judicieusement
level.W2L3=Pensez à l'avance
level.W2L4=Continuer
level.W2L5=Chemins invisibles
level.W3L1=Courants
level.W3L2=Final ventoso
level.W3L3=Saut en longueur
level.W3L4=De l'autre côté
level.W3L5=Choisissez votre défi
level.W4L1=Ne pas tomber
level.W4L2=Sérieusement, avec précaution
level.W4L3=Ne pas regarder vers le bas
level.W4L4=Au-dessus
level.W4L5=Vertige

# Not yet seen in the game
endOfGame.watchAd=Regarder l'annonce
endOfGame.message=Félicitations!
endOfGame.message=Vous avez terminé tous les niveaux actuellement disponibles.
endOfGame.message=S'il vous plaît prendre le temps de contribuer au développement de mon jeu principal: Rolling Light.
endOfGame.visitSite=Visiter Web
endOfGame.levelMenu=Menu de niveaux
