﻿Short description:
--------------------
Ziehen, Zielen, und Los! Kannst du das Ende erreichen?

Full description:
------------------

Rolling Light Throw ist ein kleines Puzzelspiel in dem du deinen Ball aus Licht wirfst, um mehr Licht einzufangen. Jeder Wurf kostet Licht, welches du versuchst, wieder aufzufüllen. Am Ende des Levels ist ein goldenes Ziel. Dein verbleibendes Licht wird zu deinem Punktestand hinzugezählt.

Rolling Light Throw benutzt lizenzfreie Musik. Die Komponisten werden eingeblendet.

Coming soon:
* Mehr Levels -- jede Woche neu!
* Leaderboards -- Versuche, deine Freunde zu schlagen!
* Mehr?!