﻿Short description:
--------------------
Arrossega, Apunta, Deixa anar. Podràs arribar al final?

Full description:
------------------

Rolling Light Throw es un petit joc on llances una bola per tal de aconseguir peces de llum. Amb cada llançament gastes llum, que pots recuperar aconseguint més peces. Al final de cada nivell hi ha una peça daurada. La puntuació dependrà de la llum restant al final.

Rolling Light Throw fa servir música de lliure distribució. La informació de l’autor es pot veure en qualsevol moment obrint el menú de pausa, i quan el joc canvia de pista.

Pròximament:
* Més nivells -- Nous cada setmana!
* Classificacions -- Intenta superar els teus amics!
* Y més sorpreses per anunciar